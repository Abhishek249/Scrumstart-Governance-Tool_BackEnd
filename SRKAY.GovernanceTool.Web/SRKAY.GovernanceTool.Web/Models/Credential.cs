﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SRKAY.GovernanceTool.Web.Models

{
    public class Credential
    {
        [Required]
        public string EmailId{ get; set; }

        public string Password{ get; set; }
        public bool IsManualLogin{ get; set; }
    }
}
