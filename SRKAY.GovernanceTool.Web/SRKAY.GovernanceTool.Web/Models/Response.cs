﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SRKAY.GovernanceTool.Web.Models
{
    public class Response
    {

        public object Result{ get; set; }
        public string Message{ get; set; }
        public bool Success { get; set; }

    }
}
