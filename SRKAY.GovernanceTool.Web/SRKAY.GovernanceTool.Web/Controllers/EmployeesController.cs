﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SRKAY.GovernanceTool.Entities;
using SRKAY.GovernanceTool.Service.Employee;

namespace SRKAY.GovernanceTool.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Employees")]
    public class EmployeesController : Controller
    {
        private readonly IEmployeeService _employeeService;
        public EmployeesController(IEmployeeService employeeService)
        {
            this._employeeService = employeeService;
        }
        [HttpGet]
        [Route("")]
        [Route("ViewEmployees")]
        public JsonResult GetEmployees()
        {
             return Json(_employeeService.GetEmployees());
        }

        [HttpPost]
        [Route("AddEmployee")]
        public IActionResult AddEmployee([FromBody] UserMaster employee)
        {
            _employeeService.AddEmployee(employee);
            return CreatedAtRoute("GetTodo",employee);
        }

        [HttpPost("employeeId")]
        [Route("UpdateEmployee/{employeeId}")]
        public IActionResult UpdateEmployee(int employeeId,[FromBody] UserMaster employee)
        {
            _employeeService.UpdateEmployee(employeeId, employee);
            return CreatedAtRoute("GetTodo", employee);
        }

        [HttpGet]
        [Route("GetEmployee/{employeeId}")]
        public IActionResult GetEmployeeById(int employeeId)
        {
            if (_employeeService.GetEmployeeById(employeeId).Count() == 0)
            {
                return NotFound();
            }
            return new ObjectResult(_employeeService.GetEmployeeById(employeeId));
        }
    }
}