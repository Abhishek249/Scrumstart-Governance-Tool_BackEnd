﻿using Microsoft.AspNetCore.Mvc;
using SRKAY.GovernanceTool.Service.Authentication;
using SRKAY.GovernanceTool.Web.Models;

namespace SRKAY.GovernanceTool.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Authentication")]
    public class AuthenticationController : Controller
    {
        private readonly IAuthService _authService;
        public AuthenticationController(IAuthService authService)
        {
            this._authService = authService;
        }

        [Route("login")]
        [HttpPost]
        public IActionResult Login([FromBody] Credential credentials)
        {
            if (ModelState.IsValid)
            {
                var data = _authService.Login(credentials.EmailId, credentials.Password, credentials.IsManualLogin);
                if (data.success)
                {
                    return Ok(new Response
                    { Message = data.message, Result = data.userMaster, Success = data.success });
                }
                return NotFound(new Response
                {
                    Message = data.message,
                    Result = data.userMaster,
                    Success = data.success
                });
            }
            return BadRequest();
        }

    }
}