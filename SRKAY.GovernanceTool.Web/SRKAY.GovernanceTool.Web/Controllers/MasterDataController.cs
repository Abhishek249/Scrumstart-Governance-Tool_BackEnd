﻿using Microsoft.AspNetCore.Mvc;
using SRKAY.GovernanceTool.Entities;
using SRKAY.GovernanceTool.Service.Location;

namespace SRKAY.GovernanceTool.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/MasterData")]
    public class MasterDataController : Controller
    {
        private readonly IMasterDataService _masterDataService;
        public MasterDataController(IMasterDataService masterDataService)
        {
            this._masterDataService = masterDataService;
        }
        [HttpGet]
        [Route("Countries")]
        public JsonResult GetAllCountries()
        {
          return Json(_masterDataService.GetAllCountries());
        }

        [HttpGet]
        [Route("Status")]
        public JsonResult GetAllStatus()
        {
            return Json(_masterDataService.GetAllStatus());
        }

        [HttpGet]
        [Route("Bands")]
        public JsonResult GetBands()
        {
            return Json(_masterDataService.GetBands());
        }

        [HttpGet]
        [Route("Cities/{stateId}")]
        public JsonResult GetCitiesByStateId(int stateId)
        {
            return Json(_masterDataService.GetCitiesByStateId(stateId));
        }

        [HttpGet]
        [Route("Domains")]
        public JsonResult GetDomains()
        {
            return Json(_masterDataService.GetDomains());
        }

        [HttpGet]
        [Route("Industries")]
        public JsonResult GetIndustries()
        {
            return Json(_masterDataService.GetIndustries());
        }

        [HttpGet]
        [Route("ProjectPhases")]
        public JsonResult GetProjectPhases()
        {
            return Json(_masterDataService.GetProjectPhases());
        }

        [HttpGet]
        [Route("Roles")]
        public JsonResult GetRoles()
        {
            return Json(_masterDataService.GetRoles());
        }

        [HttpGet]
        [Route("Skills")]
        public JsonResult GetSkills()
        {
            return Json(_masterDataService.GetSkills());
        }

        [HttpGet]
        [Route("States/{countryId}")]
        public JsonResult GetStatesByCountryId(int countryId)
        {
            return Json(_masterDataService.GetStatesByCountryId(countryId));
        }

        [HttpGet]
        [Route("UserTypes")]
        public JsonResult GetUserTypes()
        {
            return Json(_masterDataService.GetUserTypes());
        }

        [HttpGet]
        [Route("Menus")]
        public JsonResult GetMenus()
        {
            return Json(_masterDataService.GetMenus());
        }

        [HttpGet]
        [Route("Department")]
        public JsonResult GetDepartment()
        {
            return Json(_masterDataService.GetDepartment());
        }
    }
}
