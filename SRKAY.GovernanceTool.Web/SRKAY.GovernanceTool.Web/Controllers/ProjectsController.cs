﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SRKAY.GovernanceTool.Service.Project;

namespace SRKAY.GovernanceTool.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/Projects")]
    public class ProjectsController : Controller
    {
        private readonly IProjectService _projectService;
        public ProjectsController(IProjectService projectService)
        {
            this._projectService = projectService;
        }
        [HttpGet]
        public JsonResult GetProjects()
        {
            return Json(_projectService.GetProjects());
        }

        [HttpGet]
        [Route("{id:int}")]
        public JsonResult GetProject(int id)
        {
            return Json(_projectService.GetProject(id));
        }
    }
}