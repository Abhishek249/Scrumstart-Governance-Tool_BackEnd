﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SRKAY.GovernanceTool.Data.UnitOfWork;
using SRKAY.GovernanceTool.Entities;
using SRKAY.GovernanceTool.Service.Authentication;
using SRKAY.GovernanceTool.Service.Location;
using SRKAY.GovernanceTool.Service.Common;
using SRKAY.GovernanceTool.Service.Project;
using SRKAY.GovernanceTool.Service.Employee;

namespace SRKAY.GovernanceTool.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddTransient<IMasterDataService,MasterDataService>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<IProjectService, ProjectService>();
            services.AddTransient<IAuthService, AuthService>();

            services.AddMvc();
          ModelDomainMapper.CreateMap();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
