﻿using System.Collections.Generic;
using System.Linq;
using SRKAY.GovernanceTool.Data.UnitOfWork;
using SRKAY.GovernanceTool.Entities;

namespace SRKAY.GovernanceTool.Service.Authentication
{
    public class AuthService : IAuthService
    {
        private readonly IUnitOfWork _unitOfWork;
        public AuthService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public (UserMaster userMaster, bool success, string message) Login(string emailId, string password, bool isManualLogin)
        {
            if (isManualLogin)
            {
                var userToLogin = _unitOfWork.UserRepository.FindBy(u =>
                    u.Email.ToLower().Equals(emailId.ToLower()) && u.Password.Equals(password) && u.IsActive.Equals(true)).SingleOrDefault();
                var loggedInUser = AutoMapper.Mapper.Map<UserMaster>(userToLogin);
                if (loggedInUser!=null)
                {
                    return (loggedInUser, true, "success");
                }

                return (null, false, "Invalid EmailId or Password");
            }
            else
            {
                var loggedInUser = AutoMapper.Mapper.Map<UserMaster>(_unitOfWork.UserRepository.FindBy(u => u.Email.ToLower().Equals(emailId.ToLower())));
                if (loggedInUser != null)
                {
                    return (loggedInUser, true, "success");
                }

                return (null, false, "Invalid Email. Please try manual login or contact admin.");
            }
        }
    }
}
