﻿using System.Collections.Generic;
using SRKAY.GovernanceTool.Entities;


namespace SRKAY.GovernanceTool.Service.Authentication
{
    public interface IAuthService
    {
        (UserMaster userMaster,bool success,string message) Login(string emailId, string password, bool isManualLogin);
    }
}
