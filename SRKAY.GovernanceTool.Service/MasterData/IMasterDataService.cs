﻿using System;
using System.Collections.Generic;
using System.Text;
using  SRKAY.GovernanceTool.Entities;
namespace SRKAY.GovernanceTool.Service.Location
{
    public interface IMasterDataService
    {
        IEnumerable<CountryMaster> GetAllCountries();
        IEnumerable<StateMaster> GetStatesByCountryId(int countryId);
        IEnumerable<CityMaster> GetCitiesByStateId(int stateId);
        IEnumerable<RoleMaster> GetRoles();
        IEnumerable<SkillsMaster> GetSkills();
        IEnumerable<DomainMaster> GetDomains();
        IEnumerable<BandMaster> GetBands();
        IEnumerable<UserTypeMaster> GetUserTypes();
        IEnumerable<IndustryMaster> GetIndustries();
        IEnumerable<ProjectPhaseMaster> GetProjectPhases();
        IEnumerable<StatusMaster> GetAllStatus();
        IEnumerable<MenuMaster> GetMenus();
        IEnumerable<DepartmentMaster> GetDepartment();
    }
}
