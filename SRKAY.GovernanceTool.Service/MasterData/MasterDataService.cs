﻿using AutoMapper;
using SRKAY.GovernanceTool.Data.UnitOfWork;
using SRKAY.GovernanceTool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SRKAY.GovernanceTool.Service.Location
{
    public class MasterDataService: IMasterDataService
    {
        private readonly IUnitOfWork _unitOfWork;
        private object addedEmployee;

        public MasterDataService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<CountryMaster> GetAllCountries()
        {
            return AutoMapper.Mapper.Map<IEnumerable<CountryMaster>>(_unitOfWork.CountryRepository.GetAll().ToList());
        }

        public IEnumerable<StatusMaster> GetAllStatus()
        {
            return AutoMapper.Mapper.Map<IEnumerable<StatusMaster>>(_unitOfWork.StatusRepository.GetAll().ToList());
        }

        public IEnumerable<BandMaster> GetBands()
        {
            return AutoMapper.Mapper.Map<IEnumerable<BandMaster>>(_unitOfWork.BandRepository.GetAll().ToList());
        }

        public IEnumerable<CityMaster> GetCitiesByStateId(int stateId)
        {
            return AutoMapper.Mapper.Map<IEnumerable<CityMaster>>(_unitOfWork.CityRepository.FindBy(e=>e.StateId==stateId).ToList());
        }

        public IEnumerable<DomainMaster> GetDomains()
        {
            return AutoMapper.Mapper.Map<IEnumerable<DomainMaster>>(_unitOfWork.DomainRepository.GetAll().ToList());
        }

        public IEnumerable<IndustryMaster> GetIndustries()
        {
            return AutoMapper.Mapper.Map<IEnumerable<IndustryMaster>>(_unitOfWork.IndustryRepository.GetAll().ToList());
        }

        public IEnumerable<ProjectPhaseMaster> GetProjectPhases()
        {
            return AutoMapper.Mapper.Map<IEnumerable<ProjectPhaseMaster>>(_unitOfWork.ProjectPhaseRepository.GetAll().ToList());
        }

        public IEnumerable<RoleMaster> GetRoles()
        {
            return AutoMapper.Mapper.Map<IEnumerable<RoleMaster>>(_unitOfWork.RoleRepository.GetAll().ToList());
        }

        public IEnumerable<SkillsMaster> GetSkills()
        {
            return AutoMapper.Mapper.Map<IEnumerable<SkillsMaster>>(_unitOfWork.SkillsRepository.GetAll().ToList());
        }

        public IEnumerable<StateMaster> GetStatesByCountryId(int countryId)
        {
            return AutoMapper.Mapper.Map<IEnumerable<StateMaster>>(_unitOfWork.StateRepository.FindBy(e => e.CountryId == countryId).ToList());
        }

        public IEnumerable<UserTypeMaster> GetUserTypes()
        {
            return AutoMapper.Mapper.Map<IEnumerable<UserTypeMaster>>(_unitOfWork.UserTypeRepository.GetAll().ToList());
        }

        public IEnumerable<MenuMaster> GetMenus()
        {
            return AutoMapper.Mapper.Map<IEnumerable<MenuMaster>>(_unitOfWork.MenuRepository.GetAll().ToList());
        }

        public IEnumerable<DepartmentMaster> GetDepartment()
        {
            return AutoMapper.Mapper.Map<IEnumerable<DepartmentMaster>>(_unitOfWork.DepartmentRepository.GetAll().ToList());
        }
    }
}