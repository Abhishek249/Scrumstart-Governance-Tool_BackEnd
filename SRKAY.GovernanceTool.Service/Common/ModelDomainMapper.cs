﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Service.Common
{
    public static class ModelDomainMapper
    {
        public static void CreateMap()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<SRKAY.GovernanceTool.Entities.CountryMaster, SRKAY.GovernanceTool.Data.Models.Countrymaster>();
                cfg.CreateMap<SRKAY.GovernanceTool.Entities.UserMaster, SRKAY.GovernanceTool.Data.Models.Usermaster>();
                
                /* etc */
            });
        }
    }
}
