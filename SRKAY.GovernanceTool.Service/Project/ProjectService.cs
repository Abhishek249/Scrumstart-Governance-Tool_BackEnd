﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SRKAY.GovernanceTool.Data.Models;
using SRKAY.GovernanceTool.Data.UnitOfWork;
using SRKAY.GovernanceTool.Entities;

namespace SRKAY.GovernanceTool.Service.Project
{
   public class ProjectService:IProjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProjectService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public IEnumerable<ProjectDetails> GetProjects()
        {
            return AutoMapper.Mapper.Map<IEnumerable<ProjectDetails>>(_unitOfWork.ProjectRepository.GetAll().ToList());
        }
        public IEnumerable<ProjectDetails> GetProject(int id)
        {
            return AutoMapper.Mapper.Map<IEnumerable<ProjectDetails>>(_unitOfWork.ProjectRepository.FindBy(e => e.ProjectId == id).ToList());
        }
    }
}
