﻿using System;
using System.Collections.Generic;
using System.Text;
using SRKAY.GovernanceTool.Entities;

namespace SRKAY.GovernanceTool.Service.Project
{
    public interface IProjectService
    {
        IEnumerable<ProjectDetails> GetProjects();
        IEnumerable<ProjectDetails> GetProject(int id);
    }
}
