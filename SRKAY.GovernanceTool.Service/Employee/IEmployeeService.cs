﻿using System.Collections.Generic;
using SRKAY.GovernanceTool.Entities;


namespace SRKAY.GovernanceTool.Service.Employee
{
    public interface IEmployeeService
    {
        IEnumerable<UserMaster> GetEmployees();
        UserMaster AddEmployee(UserMaster employee);
        UserMaster UpdateEmployee(int employeeId, UserMaster employee);
        IEnumerable<UserMaster> GetEmployeeById(int employeeId);
    }
}
