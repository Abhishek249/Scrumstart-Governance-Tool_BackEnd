﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SRKAY.GovernanceTool.Data.Models;
using SRKAY.GovernanceTool.Data.UnitOfWork;
using SRKAY.GovernanceTool.Entities;

namespace SRKAY.GovernanceTool.Service.Employee
{
   public class EmployeeService : IEmployeeService
    {
        private readonly IUnitOfWork _unitOfWork;
        private object updatedEmployee;

        public EmployeeService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        
        public IEnumerable<UserMaster> GetEmployees()
        {
            return AutoMapper.Mapper.Map<IEnumerable<UserMaster>>(_unitOfWork.UserRepository.GetAll().ToList());
        }

        public UserMaster AddEmployee(UserMaster employee)
        {
            
            var addedEmployee = _unitOfWork.UserRepository.Add(Mapper.Map<SRKAY.GovernanceTool.Data.Models.Usermaster>(employee));
            _unitOfWork.Save();
            return Mapper.Map<UserMaster>(addedEmployee);
        }

        public IEnumerable<UserMaster> GetEmployeeById(int employeeId)
        {
            return AutoMapper.Mapper.Map<IEnumerable<UserMaster>>(_unitOfWork.UserRepository.FindBy(e => e.UserId == employeeId).ToList());
        }
        public UserMaster UpdateEmployee(int employeeId, UserMaster employee)
        {
            //var addedEmployee;
            try
            {
                var user = AutoMapper.Mapper.Map<IEnumerable<UserMaster>>(_unitOfWork.UserRepository.FindBy(e => e.UserId == employeeId).FirstOrDefault());

                if (user != null)
                {
                    updatedEmployee = _unitOfWork.UserRepository.Edit(Mapper.Map<SRKAY.GovernanceTool.Data.Models.Usermaster>(user));
                    _unitOfWork.Save();
                }
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }
            
            return Mapper.Map<UserMaster>(updatedEmployee);
        }
    }
}
