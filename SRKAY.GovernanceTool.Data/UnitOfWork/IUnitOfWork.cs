﻿using System;
using SRKAY.GovernanceTool.Data.Models;
using SRKAY.GovernanceTool.Data.Repository;


namespace SRKAY.GovernanceTool.Data.UnitOfWork
{
    public interface IUnitOfWork :IDisposable
    {
        IGenericRepository<Countrymaster> CountryRepository { get; }
        IGenericRepository<Statemaster> StateRepository { get; }
        IGenericRepository<Citymaster> CityRepository { get; }
        IGenericRepository<Statusmaster> StatusRepository { get; }
        IGenericRepository<Rolemaster> RoleRepository { get; }
        IGenericRepository<Skillsmaster> SkillsRepository { get; }
        IGenericRepository<Bandmaster> BandRepository { get; }
        IGenericRepository<Domainmaster> DomainRepository { get; }
        IGenericRepository<Projectphasemaster> ProjectPhaseRepository { get; }
        IGenericRepository<Usertypemaster> UserTypeRepository { get; }
        IGenericRepository<Industrymaster> IndustryRepository { get; }
        IGenericRepository<Usermaster> UserRepository { get; }
        IGenericRepository<Projectdetails> ProjectRepository { get; }
        IGenericRepository<Menumaster> MenuRepository { get; }
        IGenericRepository<Departmentmaster> DepartmentRepository { get; }

        void Save();
    }
}
