﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using SRKAY.GovernanceTool.Data.Models;
using SRKAY.GovernanceTool.Data.Repository;


namespace SRKAY.GovernanceTool.Data.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        private DbTransaction _transaction;
        private IGenericRepository<Countrymaster> countryRepository;
        private IGenericRepository<Statemaster> stateRepository;
        private IGenericRepository<Citymaster> cityRepository;
        private IGenericRepository<Statusmaster> statusRepository;
        private IGenericRepository<Rolemaster> roleRepository;
        private IGenericRepository<Skillsmaster> skillsRepository;
        private IGenericRepository<Bandmaster> bandRepository;
        private IGenericRepository<Domainmaster> domainRepository;
        private IGenericRepository<Projectphasemaster> projectPhaseRepository;
        private IGenericRepository<Usertypemaster> userTypeRepository;
        private IGenericRepository<Industrymaster> industryRepository;
        private IGenericRepository<Usermaster> userRepository;
        private IGenericRepository<Projectdetails> projectRepository;
        private IGenericRepository<Menumaster> menuRepository;
        private IGenericRepository<Departmentmaster> departmentRepository;


        public UnitOfWork()
        {
            this._context = new ScrumstartToolContext();
        }
        public IGenericRepository<Countrymaster> CountryRepository => countryRepository ?? (this.countryRepository = new GenericRepository<Countrymaster>(_context));
        public IGenericRepository<Statemaster> StateRepository => stateRepository ?? (this.stateRepository = new GenericRepository<Statemaster>(_context));
        public IGenericRepository<Citymaster> CityRepository => cityRepository ?? (this.cityRepository = new GenericRepository<Citymaster>(_context));
        public IGenericRepository<Statusmaster> StatusRepository => statusRepository ?? (this.statusRepository = new GenericRepository<Statusmaster>(_context));
        public IGenericRepository<Rolemaster> RoleRepository => roleRepository ?? (this.roleRepository = new GenericRepository<Rolemaster>(_context));
        public IGenericRepository<Skillsmaster> SkillsRepository => skillsRepository ?? (this.skillsRepository = new GenericRepository<Skillsmaster>(_context));
        public IGenericRepository<Bandmaster> BandRepository => bandRepository ?? (this.bandRepository = new GenericRepository<Bandmaster>(_context));
        public IGenericRepository<Domainmaster> DomainRepository => domainRepository ?? (this.domainRepository = new GenericRepository<Domainmaster>(_context));
        public IGenericRepository<Projectphasemaster> ProjectPhaseRepository => projectPhaseRepository ?? (this.projectPhaseRepository = new GenericRepository<Projectphasemaster>(_context));
        public IGenericRepository<Industrymaster> IndustryRepository => industryRepository ?? (this.industryRepository = new GenericRepository<Industrymaster>(_context));
        public IGenericRepository<Usertypemaster> UserTypeRepository => userTypeRepository ?? (this.userTypeRepository = new GenericRepository<Usertypemaster>(_context));
        public IGenericRepository<Usermaster> UserRepository => userRepository ?? (this.userRepository = new GenericRepository<Usermaster>(_context));
        public IGenericRepository<Projectdetails> ProjectRepository => projectRepository ?? (this.projectRepository = new GenericRepository<Projectdetails>(_context));
        public IGenericRepository<Menumaster> MenuRepository => menuRepository ?? (this.menuRepository = new GenericRepository<Menumaster>(_context));
        public IGenericRepository<Departmentmaster> DepartmentRepository => departmentRepository ?? (this.departmentRepository = new GenericRepository<Departmentmaster>(_context));
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (Exception exception)
            {
                //Log Exception
                //throw;
                Console.WriteLine(exception);
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed && disposing)
            {
                _context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}



