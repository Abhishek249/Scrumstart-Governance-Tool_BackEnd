﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Menuroles
    {
        public sbyte MenuId { get; set; }
        public sbyte RoleId { get; set; }

        public Menumaster Menu { get; set; }
        public Rolemaster Role { get; set; }
    }
}
