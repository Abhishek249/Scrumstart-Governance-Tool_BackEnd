﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Usertypemaster
    {
        public Usertypemaster()
        {
            Usermaster = new HashSet<Usermaster>();
        }

        public sbyte UserTypeId { get; set; }
        public string UserType { get; set; }

        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
