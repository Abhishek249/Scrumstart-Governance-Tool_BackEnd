﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projecttechnologies
    {
        public int ProjectId { get; set; }
        public sbyte SkillId { get; set; }

        public Projectdetails Project { get; set; }
        public Skillsmaster Skill { get; set; }
    }
}
