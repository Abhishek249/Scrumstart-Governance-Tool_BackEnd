﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Usermaster
    {
        public Usermaster()
        {
            InverseManager = new HashSet<Usermaster>();
            ProjectdetailsAddedByNavigation = new HashSet<Projectdetails>();
            ProjectdetailsUpdatedByNavigation = new HashSet<Projectdetails>();
            Projectresources = new HashSet<Projectresources>();
            ResourcecontrolpersmissionCreatedByNavigation = new HashSet<Resourcecontrolpersmission>();
            ResourcecontrolpersmissionUpdatedByNavigation = new HashSet<Resourcecontrolpersmission>();
            ResourcepersmissionCreatedByNavigation = new HashSet<Resourcepersmission>();
            ResourcepersmissionUpdatedByNavigation = new HashSet<Resourcepersmission>();
            Userdomains = new HashSet<Userdomains>();
            Userskills = new HashSet<Userskills>();
            Visadetails = new HashSet<Visadetails>();
        }

        public int UserId { get; set; }
        public string PayRollId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ContactNo { get; set; }
        public sbyte RoleId { get; set; }
        public float? Experience { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public DateTime? BirthDate { get; set; }
        public sbyte UserTypeId { get; set; }
        public sbyte? AgencyId { get; set; }
        public int LocationId { get; set; }
        public sbyte BandId { get; set; }
        public int? ManagerId { get; set; }
        public bool? IsBillable { get; set; }
        public DateTime? DateOfJoining { get; set; }
        public DateTime? DateOfExit { get; set; }
        public string Post { get; set; }
        public sbyte? DepartmentId { get; set; }
        public bool IsActive { get; set; }

        public Agencymaster Agency { get; set; }
        public Bandmaster Band { get; set; }
        public Departmentmaster Department { get; set; }
        public Locationmaster Location { get; set; }
        public Usermaster Manager { get; set; }
        public Rolemaster Role { get; set; }
        public Usertypemaster UserType { get; set; }
        public ICollection<Usermaster> InverseManager { get; set; }
        public ICollection<Projectdetails> ProjectdetailsAddedByNavigation { get; set; }
        public ICollection<Projectdetails> ProjectdetailsUpdatedByNavigation { get; set; }
        public ICollection<Projectresources> Projectresources { get; set; }
        public ICollection<Resourcecontrolpersmission> ResourcecontrolpersmissionCreatedByNavigation { get; set; }
        public ICollection<Resourcecontrolpersmission> ResourcecontrolpersmissionUpdatedByNavigation { get; set; }
        public ICollection<Resourcepersmission> ResourcepersmissionCreatedByNavigation { get; set; }
        public ICollection<Resourcepersmission> ResourcepersmissionUpdatedByNavigation { get; set; }
        public ICollection<Userdomains> Userdomains { get; set; }
        public ICollection<Userskills> Userskills { get; set; }
        public ICollection<Visadetails> Visadetails { get; set; }
    }
}
