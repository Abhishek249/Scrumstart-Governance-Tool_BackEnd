﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Statemaster
    {
        public Statemaster()
        {
            Citymaster = new HashSet<Citymaster>();
        }

        public int StateId { get; set; }
        public string StateName { get; set; }
        public int CountryId { get; set; }

        public Countrymaster Country { get; set; }
        public ICollection<Citymaster> Citymaster { get; set; }
    }
}
