﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projectresources
    {
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public sbyte ProjectRoleId { get; set; }

        public Projectdetails Project { get; set; }
        public Projectsrolemaster ProjectRole { get; set; }
        public Usermaster User { get; set; }
    }
}
