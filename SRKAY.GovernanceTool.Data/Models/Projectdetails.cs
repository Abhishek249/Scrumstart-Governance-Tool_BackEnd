﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projectdetails
    {
        public Projectdetails()
        {
            Projectresources = new HashSet<Projectresources>();
            Projectstatuslogs = new HashSet<Projectstatuslogs>();
            Projecttechnologies = new HashSet<Projecttechnologies>();
        }

        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public int ClientId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public sbyte? ProjectPhaseId { get; set; }
        public DateTime AddedDateTime { get; set; }
        public int AddedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? PlannedDeliveryDate { get; set; }
        public sbyte PriorityId { get; set; }

        public Usermaster AddedByNavigation { get; set; }
        public Clientdetails Client { get; set; }
        public Prioritymaster Priority { get; set; }
        public Projectphasemaster ProjectPhase { get; set; }
        public Usermaster UpdatedByNavigation { get; set; }
        public ICollection<Projectresources> Projectresources { get; set; }
        public ICollection<Projectstatuslogs> Projectstatuslogs { get; set; }
        public ICollection<Projecttechnologies> Projecttechnologies { get; set; }
    }
}
