﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Clientaddress
    {
        public int AddressId { get; set; }
        public int ClientId { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public bool IsPrimaryAddress { get; set; }

        public Clientdetails Client { get; set; }
    }
}
