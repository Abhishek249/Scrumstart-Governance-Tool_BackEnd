﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Clientcontactdetails
    {
        public int ContactId { get; set; }
        public int ClientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string EmailId { get; set; }
        public sbyte RoleId { get; set; }
        public bool IsPrimaryContact { get; set; }

        public Clientdetails Client { get; set; }
        public Rolemaster Role { get; set; }
    }
}
