﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Rolemaster
    {
        public Rolemaster()
        {
            Clientcontactdetails = new HashSet<Clientcontactdetails>();
            Menuroles = new HashSet<Menuroles>();
            Resourcecontrolpersmission = new HashSet<Resourcecontrolpersmission>();
            Resourcepersmission = new HashSet<Resourcepersmission>();
            Usermaster = new HashSet<Usermaster>();
        }

        public sbyte RoleId { get; set; }
        public string RoleName { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Clientcontactdetails> Clientcontactdetails { get; set; }
        public ICollection<Menuroles> Menuroles { get; set; }
        public ICollection<Resourcecontrolpersmission> Resourcecontrolpersmission { get; set; }
        public ICollection<Resourcepersmission> Resourcepersmission { get; set; }
        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
