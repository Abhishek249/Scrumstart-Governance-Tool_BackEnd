﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projectsrolemaster
    {
        public Projectsrolemaster()
        {
            Projectresources = new HashSet<Projectresources>();
        }

        public sbyte ProjectRoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Projectresources> Projectresources { get; set; }
    }
}
