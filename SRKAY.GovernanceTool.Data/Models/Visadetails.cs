﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Visadetails
    {
        public sbyte VisaDetailsId { get; set; }
        public int UserId { get; set; }
        public int CountryId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Countrymaster Country { get; set; }
        public Usermaster User { get; set; }
    }
}
