﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Clientdetails
    {
        public Clientdetails()
        {
            Clientaddress = new HashSet<Clientaddress>();
            Clientcontactdetails = new HashSet<Clientcontactdetails>();
            Projectdetails = new HashSet<Projectdetails>();
        }

        public int ClientId { get; set; }
        public string UserName { get; set; }
        public string CompanyName { get; set; }
        public sbyte IndustryId { get; set; }
        public bool IsActive { get; set; }

        public Industrymaster Industry { get; set; }
        public ICollection<Clientaddress> Clientaddress { get; set; }
        public ICollection<Clientcontactdetails> Clientcontactdetails { get; set; }
        public ICollection<Projectdetails> Projectdetails { get; set; }
    }
}
