﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Menumaster
    {
        public Menumaster()
        {
            Menuroles = new HashSet<Menuroles>();
        }

        public sbyte MenuId { get; set; }
        public string MenuName { get; set; }
        public bool IsActive { get; set; }
        public sbyte ParentId { get; set; }

        public ICollection<Menuroles> Menuroles { get; set; }
    }
}
