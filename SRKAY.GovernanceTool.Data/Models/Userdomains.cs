﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Userdomains
    {
        public int UserId { get; set; }
        public sbyte DomainId { get; set; }

        public Domainmaster Domain { get; set; }
        public Usermaster User { get; set; }
    }
}
