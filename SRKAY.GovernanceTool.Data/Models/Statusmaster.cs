﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Statusmaster
    {
        public Statusmaster()
        {
            Projectstatuslogs = new HashSet<Projectstatuslogs>();
        }

        public sbyte StatusId { get; set; }
        public string Status { get; set; }

        public ICollection<Projectstatuslogs> Projectstatuslogs { get; set; }
    }
}
