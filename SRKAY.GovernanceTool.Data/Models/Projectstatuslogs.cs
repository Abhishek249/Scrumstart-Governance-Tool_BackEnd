﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projectstatuslogs
    {
        public int ProjectId { get; set; }
        public sbyte StatusId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Comments { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }

        public Projectdetails Project { get; set; }
        public Statusmaster Status { get; set; }
    }
}
