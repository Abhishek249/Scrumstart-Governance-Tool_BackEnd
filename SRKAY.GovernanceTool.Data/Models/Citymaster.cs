﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Citymaster
    {
        public Citymaster()
        {
            Agencymaster = new HashSet<Agencymaster>();
            Locationmaster = new HashSet<Locationmaster>();
        }

        public int CityId { get; set; }
        public string CityName { get; set; }
        public int StateId { get; set; }

        public Statemaster State { get; set; }
        public ICollection<Agencymaster> Agencymaster { get; set; }
        public ICollection<Locationmaster> Locationmaster { get; set; }
    }
}
