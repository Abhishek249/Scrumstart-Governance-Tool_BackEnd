﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Resourcepersmission
    {
        public sbyte ResourceId { get; set; }
        public sbyte RoleId { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public bool IsAllowed { get; set; }

        public Usermaster CreatedByNavigation { get; set; }
        public Resourcemaster Resource { get; set; }
        public Rolemaster Role { get; set; }
        public Usermaster UpdatedByNavigation { get; set; }
    }
}
