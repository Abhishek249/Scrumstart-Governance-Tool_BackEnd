﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Projectphasemaster
    {
        public Projectphasemaster()
        {
            Projectdetails = new HashSet<Projectdetails>();
        }

        public sbyte ProjectPhaseId { get; set; }
        public string ProjectPhase { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Projectdetails> Projectdetails { get; set; }
    }
}
