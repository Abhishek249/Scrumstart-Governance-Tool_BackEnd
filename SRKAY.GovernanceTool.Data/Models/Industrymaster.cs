﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Industrymaster
    {
        public Industrymaster()
        {
            Clientdetails = new HashSet<Clientdetails>();
        }

        public sbyte IndustryId { get; set; }
        public string IndustryType { get; set; }

        public ICollection<Clientdetails> Clientdetails { get; set; }
    }
}
