﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Resourcecontrol
    {
        public Resourcecontrol()
        {
            Resourcecontrolpersmission = new HashSet<Resourcecontrolpersmission>();
        }

        public int ControlId { get; set; }
        public string ControlName { get; set; }
        public string ControlDesc { get; set; }
        public sbyte ResourceId { get; set; }
        public bool IsActive { get; set; }

        public Resourcemaster Resource { get; set; }
        public ICollection<Resourcecontrolpersmission> Resourcecontrolpersmission { get; set; }
    }
}
