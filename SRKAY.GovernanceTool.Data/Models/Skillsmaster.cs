﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Skillsmaster
    {
        public Skillsmaster()
        {
            Projecttechnologies = new HashSet<Projecttechnologies>();
            Userskills = new HashSet<Userskills>();
        }

        public sbyte SkillId { get; set; }
        public string Skill { get; set; }

        public ICollection<Projecttechnologies> Projecttechnologies { get; set; }
        public ICollection<Userskills> Userskills { get; set; }
    }
}
