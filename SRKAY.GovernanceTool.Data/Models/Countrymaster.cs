﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Countrymaster
    {
        public Countrymaster()
        {
            Statemaster = new HashSet<Statemaster>();
            Visadetails = new HashSet<Visadetails>();
        }

        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public int? PhoneCode { get; set; }

        public ICollection<Statemaster> Statemaster { get; set; }
        public ICollection<Visadetails> Visadetails { get; set; }
    }
}
