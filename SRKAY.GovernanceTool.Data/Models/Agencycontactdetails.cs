﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Agencycontactdetails
    {
        public int ContactId { get; set; }
        public sbyte AgencyId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactNo1 { get; set; }
        public string ContactNo2 { get; set; }
        public string EmailId { get; set; }
        public bool IsPrimaryContact { get; set; }

        public Agencymaster Agency { get; set; }
    }
}
