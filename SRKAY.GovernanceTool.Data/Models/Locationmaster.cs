﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Locationmaster
    {
        public Locationmaster()
        {
            Usermaster = new HashSet<Usermaster>();
        }

        public int LocationId { get; set; }
        public int CityId { get; set; }
        public string Adreess { get; set; }
        public string ContactNo { get; set; }

        public Citymaster City { get; set; }
        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
