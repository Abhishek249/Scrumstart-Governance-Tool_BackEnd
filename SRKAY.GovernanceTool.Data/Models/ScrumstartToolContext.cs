﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class ScrumstartToolContext : DbContext
    {
        public virtual DbSet<Agencycontactdetails> Agencycontactdetails { get; set; }
        public virtual DbSet<Agencymaster> Agencymaster { get; set; }
        public virtual DbSet<Bandmaster> Bandmaster { get; set; }
        public virtual DbSet<Citymaster> Citymaster { get; set; }
        public virtual DbSet<Clientaddress> Clientaddress { get; set; }
        public virtual DbSet<Clientcontactdetails> Clientcontactdetails { get; set; }
        public virtual DbSet<Clientdetails> Clientdetails { get; set; }
        public virtual DbSet<Countrymaster> Countrymaster { get; set; }
        public virtual DbSet<Departmentmaster> Departmentmaster { get; set; }
        public virtual DbSet<Domainmaster> Domainmaster { get; set; }
        public virtual DbSet<Industrymaster> Industrymaster { get; set; }
        public virtual DbSet<Locationmaster> Locationmaster { get; set; }
        public virtual DbSet<Menumaster> Menumaster { get; set; }
        public virtual DbSet<Menuroles> Menuroles { get; set; }
        public virtual DbSet<Prioritymaster> Prioritymaster { get; set; }
        public virtual DbSet<Projectdetails> Projectdetails { get; set; }
        public virtual DbSet<Projectphasemaster> Projectphasemaster { get; set; }
        public virtual DbSet<Projectresources> Projectresources { get; set; }
        public virtual DbSet<Projectsrolemaster> Projectsrolemaster { get; set; }
        public virtual DbSet<Projectstatuslogs> Projectstatuslogs { get; set; }
        public virtual DbSet<Projecttechnologies> Projecttechnologies { get; set; }
        public virtual DbSet<Resourcecontrol> Resourcecontrol { get; set; }
        public virtual DbSet<Resourcecontrolpersmission> Resourcecontrolpersmission { get; set; }
        public virtual DbSet<Resourcemaster> Resourcemaster { get; set; }
        public virtual DbSet<Resourcepersmission> Resourcepersmission { get; set; }
        public virtual DbSet<Rolemaster> Rolemaster { get; set; }
        public virtual DbSet<Skillevel> Skillevel { get; set; }
        public virtual DbSet<Skillsmaster> Skillsmaster { get; set; }
        public virtual DbSet<Statemaster> Statemaster { get; set; }
        public virtual DbSet<Statusmaster> Statusmaster { get; set; }
        public virtual DbSet<Userdomains> Userdomains { get; set; }
        public virtual DbSet<Usermaster> Usermaster { get; set; }
        public virtual DbSet<Userskills> Userskills { get; set; }
        public virtual DbSet<Usertypemaster> Usertypemaster { get; set; }
        public virtual DbSet<Visadetails> Visadetails { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=192.168.11.70;user=scrumstart;password=ScrumStart;database=ScrumstartTool;TreatTinyAsBoolean=false");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Agencycontactdetails>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.ToTable("agencycontactdetails");

                entity.HasIndex(e => e.AgencyId)
                    .HasName("AgencyId_idx");

                entity.Property(e => e.ContactId).HasColumnType("int(11)");

                entity.Property(e => e.AgencyId).HasColumnType("tinyint(1)");

                entity.Property(e => e.ContactNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.ContactNo2).HasMaxLength(15);

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsPrimaryContact).HasColumnType("bit(4)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Agency)
                    .WithMany(p => p.Agencycontactdetails)
                    .HasForeignKey(d => d.AgencyId)
                    .HasConstraintName("AgencyId");
            });

            modelBuilder.Entity<Agencymaster>(entity =>
            {
                entity.HasKey(e => e.AgencyId);

                entity.ToTable("agencymaster");

                entity.HasIndex(e => e.CityId)
                    .HasName("CityId");

                entity.Property(e => e.AgencyId).HasColumnType("tinyint(1)");

                entity.Property(e => e.Address).HasColumnType("text");

                entity.Property(e => e.AgencyName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CityId).HasColumnType("int(11)");

                entity.Property(e => e.ContactNo)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.IsActive).HasColumnType("bit(4)");

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Agencymaster)
                    .HasForeignKey(d => d.CityId)
                    .HasConstraintName("fkcityagency");
            });

            modelBuilder.Entity<Bandmaster>(entity =>
            {
                entity.HasKey(e => e.BandId);

                entity.ToTable("bandmaster");

                entity.Property(e => e.BandId).HasColumnType("tinyint(1)");

                entity.Property(e => e.Band)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<Citymaster>(entity =>
            {
                entity.HasKey(e => e.CityId);

                entity.ToTable("citymaster");

                entity.HasIndex(e => e.StateId)
                    .HasName("CMStateId_idx");

                entity.Property(e => e.CityId).HasColumnType("int(11)");

                entity.Property(e => e.CityName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StateId).HasColumnType("int(11)");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Citymaster)
                    .HasForeignKey(d => d.StateId)
                    .HasConstraintName("fkstatemaster");
            });

            modelBuilder.Entity<Clientaddress>(entity =>
            {
                entity.HasKey(e => e.AddressId);

                entity.ToTable("clientaddress");

                entity.HasIndex(e => e.CityId)
                    .HasName("CityId")
                    .IsUnique();

                entity.HasIndex(e => e.ClientId)
                    .HasName("FKClientId_idx");

                entity.Property(e => e.AddressId).HasColumnType("int(11)");

                entity.Property(e => e.Address).HasColumnType("text");

                entity.Property(e => e.CityId).HasColumnType("int(11)");

                entity.Property(e => e.ClientId).HasColumnType("int(11)");

                entity.Property(e => e.IsPrimaryAddress).HasColumnType("bit(4)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Clientaddress)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKClientId");
            });

            modelBuilder.Entity<Clientcontactdetails>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.ToTable("clientcontactdetails");

                entity.HasIndex(e => e.ClientId)
                    .HasName("ClientId");

                entity.HasIndex(e => e.RoleId)
                    .HasName("RoleId_idx");

                entity.Property(e => e.ContactId).HasColumnType("int(11)");

                entity.Property(e => e.ClientId).HasColumnType("int(11)");

                entity.Property(e => e.ContactNo1)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.ContactNo2).HasMaxLength(15);

                entity.Property(e => e.EmailId)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsPrimaryContact).HasColumnType("bit(4)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.RoleId).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Clientcontactdetails)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("clientcontactdetails_ibfk_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Clientcontactdetails)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("RoleId");
            });

            modelBuilder.Entity<Clientdetails>(entity =>
            {
                entity.HasKey(e => e.ClientId);

                entity.ToTable("clientdetails");

                entity.HasIndex(e => e.IndustryId)
                    .HasName("IndustryId");

                entity.HasIndex(e => e.UserName)
                    .HasName("UserName_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.ClientId).HasColumnType("int(11)");

                entity.Property(e => e.CompanyName)
                    .IsRequired()
                    .HasMaxLength(256);

                entity.Property(e => e.IndustryId).HasColumnType("tinyint(4)");

                entity.Property(e => e.IsActive).HasColumnType("bit(4)");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Industry)
                    .WithMany(p => p.Clientdetails)
                    .HasForeignKey(d => d.IndustryId)
                    .HasConstraintName("clientdetails_ibfk_1");
            });

            modelBuilder.Entity<Countrymaster>(entity =>
            {
                entity.HasKey(e => e.CountryId);

                entity.ToTable("countrymaster");

                entity.Property(e => e.CountryId).HasColumnType("int(11)");

                entity.Property(e => e.CountryCode).HasColumnType("char(3)");

                entity.Property(e => e.CountryName).HasMaxLength(50);

                entity.Property(e => e.PhoneCode).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Departmentmaster>(entity =>
            {
                entity.HasKey(e => e.DepartmentId);

                entity.ToTable("departmentmaster");

                entity.Property(e => e.DepartmentId).HasColumnType("tinyint(1)");

                entity.Property(e => e.DepartmentName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<Domainmaster>(entity =>
            {
                entity.HasKey(e => e.DomainId);

                entity.ToTable("domainmaster");

                entity.Property(e => e.DomainId).HasColumnType("tinyint(1)");

                entity.Property(e => e.DomainName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Industrymaster>(entity =>
            {
                entity.HasKey(e => e.IndustryId);

                entity.ToTable("industrymaster");

                entity.Property(e => e.IndustryId).HasColumnType("tinyint(4)");

                entity.Property(e => e.IndustryType)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Locationmaster>(entity =>
            {
                entity.HasKey(e => e.LocationId);

                entity.ToTable("locationmaster");

                entity.HasIndex(e => e.CityId)
                    .HasName("LMCityId_idx");

                entity.Property(e => e.LocationId).HasColumnType("int(11)");

                entity.Property(e => e.Adreess).HasMaxLength(255);

                entity.Property(e => e.CityId).HasColumnType("int(11)");

                entity.Property(e => e.ContactNo).HasMaxLength(15);

                entity.HasOne(d => d.City)
                    .WithMany(p => p.Locationmaster)
                    .HasForeignKey(d => d.CityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fkcitylocationid");
            });

            modelBuilder.Entity<Menumaster>(entity =>
            {
                entity.HasKey(e => e.MenuId);

                entity.ToTable("menumaster");

                entity.Property(e => e.MenuId).HasColumnType("tinyint(4)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.MenuName)
                    .IsRequired()
                    .HasMaxLength(45);

                entity.Property(e => e.ParentId).HasColumnType("tinyint(4)");
            });

            modelBuilder.Entity<Menuroles>(entity =>
            {
                entity.HasKey(e => new { e.MenuId, e.RoleId });

                entity.ToTable("menuroles");

                entity.HasIndex(e => e.MenuId)
                    .HasName("MenuId");

                entity.HasIndex(e => e.RoleId)
                    .HasName("RoleId");

                entity.Property(e => e.MenuId).HasColumnType("tinyint(4)");

                entity.Property(e => e.RoleId).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Menu)
                    .WithMany(p => p.Menuroles)
                    .HasForeignKey(d => d.MenuId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("menuroles_ibfk_1");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Menuroles)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("menuroles_ibfk_2");
            });

            modelBuilder.Entity<Prioritymaster>(entity =>
            {
                entity.HasKey(e => e.PriorityId);

                entity.ToTable("prioritymaster");

                entity.Property(e => e.PriorityId).HasColumnType("tinyint(1)");

                entity.Property(e => e.PriorityLevel)
                    .IsRequired()
                    .HasMaxLength(15);
            });

            modelBuilder.Entity<Projectdetails>(entity =>
            {
                entity.HasKey(e => e.ProjectId);

                entity.ToTable("projectdetails");

                entity.HasIndex(e => e.AddedBy)
                    .HasName("AddedBy");

                entity.HasIndex(e => e.ClientId)
                    .HasName("ClientId");

                entity.HasIndex(e => e.PriorityId)
                    .HasName("PriorityId");

                entity.HasIndex(e => e.ProjectPhaseId)
                    .HasName("ProjectPhaseId");

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdatedBy");

                entity.Property(e => e.ProjectId).HasColumnType("int(11)");

                entity.Property(e => e.AddedBy).HasColumnType("int(11)");

                entity.Property(e => e.AddedDateTime).HasColumnType("datetime");

                entity.Property(e => e.ClientId).HasColumnType("int(11)");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.PlannedDeliveryDate).HasColumnType("datetime");

                entity.Property(e => e.PriorityId).HasColumnType("tinyint(1)");

                entity.Property(e => e.ProjectDescription).HasColumnType("text");

                entity.Property(e => e.ProjectName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProjectPhaseId).HasColumnType("tinyint(4)");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy).HasColumnType("int(11)");

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.AddedByNavigation)
                    .WithMany(p => p.ProjectdetailsAddedByNavigation)
                    .HasForeignKey(d => d.AddedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectdetails_ibfk_3");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.Projectdetails)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectdetails_ibfk_2");

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.Projectdetails)
                    .HasForeignKey(d => d.PriorityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectdetails_ibfk_5");

                entity.HasOne(d => d.ProjectPhase)
                    .WithMany(p => p.Projectdetails)
                    .HasForeignKey(d => d.ProjectPhaseId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("projectdetails_ibfk_1");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.ProjectdetailsUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .HasConstraintName("projectdetails_ibfk_4");
            });

            modelBuilder.Entity<Projectphasemaster>(entity =>
            {
                entity.HasKey(e => e.ProjectPhaseId);

                entity.ToTable("projectphasemaster");

                entity.Property(e => e.ProjectPhaseId).HasColumnType("tinyint(4)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.ProjectPhase)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<Projectresources>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.UserId, e.ProjectRoleId });

                entity.ToTable("projectresources");

                entity.HasIndex(e => e.ProjectId)
                    .HasName("ProjectId");

                entity.HasIndex(e => e.ProjectRoleId)
                    .HasName("ProjectRoleId");

                entity.HasIndex(e => e.UserId)
                    .HasName("UserId");

                entity.Property(e => e.ProjectId).HasColumnType("int(11)");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.ProjectRoleId).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.Projectresources)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectresources_ibfk_1");

                entity.HasOne(d => d.ProjectRole)
                    .WithMany(p => p.Projectresources)
                    .HasForeignKey(d => d.ProjectRoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectresources_ibfk_2");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Projectresources)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectresources_ibfk_3");
            });

            modelBuilder.Entity<Projectsrolemaster>(entity =>
            {
                entity.HasKey(e => e.ProjectRoleId);

                entity.ToTable("projectsrolemaster");

                entity.Property(e => e.ProjectRoleId).HasColumnType("tinyint(4)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Projectstatuslogs>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.StatusId });

                entity.ToTable("projectstatuslogs");

                entity.HasIndex(e => e.ProjectId)
                    .HasName("ProjectId");

                entity.HasIndex(e => e.StatusId)
                    .HasName("StatusId");

                entity.Property(e => e.ProjectId).HasColumnType("int(11)");

                entity.Property(e => e.StatusId).HasColumnType("tinyint(4)");

                entity.Property(e => e.Comments).HasColumnType("text");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy).HasColumnType("int(11)");

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.Projectstatuslogs)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectstatuslogs_ibfk_1");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Projectstatuslogs)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projectstatuslogs_ibfk_2");
            });

            modelBuilder.Entity<Projecttechnologies>(entity =>
            {
                entity.HasKey(e => new { e.ProjectId, e.SkillId });

                entity.ToTable("projecttechnologies");

                entity.HasIndex(e => e.ProjectId)
                    .HasName("ProjectId");

                entity.HasIndex(e => e.SkillId)
                    .HasName("SkillId");

                entity.Property(e => e.ProjectId).HasColumnType("int(11)");

                entity.Property(e => e.SkillId).HasColumnType("tinyint(4)");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.Projecttechnologies)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projecttechnologies_ibfk_1");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.Projecttechnologies)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("projecttechnologies_ibfk_2");
            });

            modelBuilder.Entity<Resourcecontrol>(entity =>
            {
                entity.HasKey(e => e.ControlId);

                entity.ToTable("resourcecontrol");

                entity.HasIndex(e => e.ResourceId)
                    .HasName("RCResourceId_idx");

                entity.Property(e => e.ControlId).HasColumnType("int(11)");

                entity.Property(e => e.ControlDesc).HasMaxLength(500);

                entity.Property(e => e.ControlName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.IsActive).HasColumnType("bit(4)");

                entity.Property(e => e.ResourceId).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Resourcecontrol)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKResourceId");
            });

            modelBuilder.Entity<Resourcecontrolpersmission>(entity =>
            {
                entity.HasKey(e => new { e.ControlId, e.RoleId });

                entity.ToTable("resourcecontrolpersmission");

                entity.HasIndex(e => e.ControlId)
                    .HasName("RCPControlId_idx");

                entity.HasIndex(e => e.CreatedBy)
                    .HasName("CreatedBy");

                entity.HasIndex(e => e.RoleId)
                    .HasName("RCPRoleId_idx");

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdatedBy");

                entity.Property(e => e.ControlId).HasColumnType("int(11)");

                entity.Property(e => e.RoleId).HasColumnType("tinyint(1)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.IsAllowed).HasColumnType("bit(4)");

                entity.Property(e => e.UpdatedBy).HasColumnType("int(11)");

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.Control)
                    .WithMany(p => p.Resourcecontrolpersmission)
                    .HasForeignKey(d => d.ControlId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRCPControlId");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ResourcecontrolpersmissionCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("resourcecontrolpersmission_ibfk_2");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Resourcecontrolpersmission)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRCPRoleId");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.ResourcecontrolpersmissionUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .HasConstraintName("resourcecontrolpersmission_ibfk_1");
            });

            modelBuilder.Entity<Resourcemaster>(entity =>
            {
                entity.HasKey(e => e.ResourceId);

                entity.ToTable("resourcemaster");

                entity.Property(e => e.ResourceId).HasColumnType("tinyint(1)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.ResourceDesc).HasMaxLength(500);

                entity.Property(e => e.ResourceName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.ResourceUrl)
                    .IsRequired()
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Resourcepersmission>(entity =>
            {
                entity.HasKey(e => new { e.ResourceId, e.RoleId });

                entity.ToTable("resourcepersmission");

                entity.HasIndex(e => e.CreatedBy)
                    .HasName("CreatedBy");

                entity.HasIndex(e => e.ResourceId)
                    .HasName("RPResourceId_idx");

                entity.HasIndex(e => e.RoleId)
                    .HasName("RPRoleId_idx");

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdaedBy");

                entity.Property(e => e.ResourceId).HasColumnType("tinyint(1)");

                entity.Property(e => e.RoleId).HasColumnType("tinyint(1)");

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.IsAllowed).HasColumnType("bit(4)");

                entity.Property(e => e.UpdatedBy).HasColumnType("int(11)");

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.ResourcepersmissionCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("resourcepersmission_ibfk_2");

                entity.HasOne(d => d.Resource)
                    .WithMany(p => p.Resourcepersmission)
                    .HasForeignKey(d => d.ResourceId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRPResourceId");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Resourcepersmission)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRPRoleId");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.ResourcepersmissionUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .HasConstraintName("resourcepersmission_ibfk_1");
            });

            modelBuilder.Entity<Rolemaster>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("rolemaster");

                entity.Property(e => e.RoleId).HasColumnType("tinyint(1)");

                entity.Property(e => e.IsActive).HasColumnType("bit(1)");

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Skillevel>(entity =>
            {
                entity.HasKey(e => e.LevelId);

                entity.ToTable("skillevel");

                entity.Property(e => e.LevelId).HasColumnType("tinyint(1)");

                entity.Property(e => e.SkillLevel)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<Skillsmaster>(entity =>
            {
                entity.HasKey(e => e.SkillId);

                entity.ToTable("skillsmaster");

                entity.Property(e => e.SkillId).HasColumnType("tinyint(1)");

                entity.Property(e => e.Skill)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Statemaster>(entity =>
            {
                entity.HasKey(e => e.StateId);

                entity.ToTable("statemaster");

                entity.HasIndex(e => e.CountryId)
                    .HasName("CountryId_idx");

                entity.Property(e => e.StateId).HasColumnType("int(11)");

                entity.Property(e => e.CountryId).HasColumnType("int(11)");

                entity.Property(e => e.StateName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Statemaster)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("fkcountryid");
            });

            modelBuilder.Entity<Statusmaster>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("statusmaster");

                entity.Property(e => e.StatusId).HasColumnType("tinyint(4)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(45);
            });

            modelBuilder.Entity<Userdomains>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.DomainId });

                entity.ToTable("userdomains");

                entity.HasIndex(e => e.DomainId)
                    .HasName("EDDomainId_idx");

                entity.HasIndex(e => e.UserId)
                    .HasName("FKUserId_idx");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.DomainId).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.Domain)
                    .WithMany(p => p.Userdomains)
                    .HasForeignKey(d => d.DomainId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKDomainId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userdomains)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUserId");
            });

            modelBuilder.Entity<Usermaster>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("usermaster");

                entity.HasIndex(e => e.AgencyId)
                    .HasName("FKAgencyId_idx");

                entity.HasIndex(e => e.BandId)
                    .HasName("BandId");

                entity.HasIndex(e => e.CreatedBy)
                    .HasName("CreatedBy");

                entity.HasIndex(e => e.DepartmentId)
                    .HasName("UMDepartmentId");

                entity.HasIndex(e => e.LocationId)
                    .HasName("LocationId_idx");

                entity.HasIndex(e => e.ManagerId)
                    .HasName("UMManagerId");

                entity.HasIndex(e => e.RoleId)
                    .HasName("RoleId_idx");

                entity.HasIndex(e => e.UpdatedBy)
                    .HasName("UpdatedBy");

                entity.HasIndex(e => e.UserTypeId)
                    .HasName("UserTypeId_idx");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.AgencyId).HasColumnType("tinyint(1)");

                entity.Property(e => e.BandId).HasColumnType("tinyint(4)");

                entity.Property(e => e.BirthDate).HasColumnType("datetime");

                entity.Property(e => e.ContactNo)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CreatedBy).HasColumnType("int(11)");

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.DateOfExit).HasColumnType("datetime");

                entity.Property(e => e.DateOfJoining).HasColumnType("datetime");

                entity.Property(e => e.DepartmentId).HasColumnType("tinyint(1)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.IsActive).HasColumnType("bit(4)");

                entity.Property(e => e.IsBillable).HasColumnType("bit(1)");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.LocationId).HasColumnType("int(11)");

                entity.Property(e => e.ManagerId).HasColumnType("int(11)");

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.PayRollId).HasMaxLength(10);

                entity.Property(e => e.Post).HasMaxLength(45);

                entity.Property(e => e.RoleId).HasColumnType("tinyint(1)");

                entity.Property(e => e.UpdatedBy).HasColumnType("int(11)");

                entity.Property(e => e.UpdatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.UserTypeId).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.Agency)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.AgencyId)
                    .HasConstraintName("FKAgencyId");

                entity.HasOne(d => d.Band)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.BandId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("usermaster_ibfk_1");

                entity.HasOne(d => d.Department)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.DepartmentId)
                    .HasConstraintName("usermaster_ibfk_3");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKLocationId");

                entity.HasOne(d => d.Manager)
                    .WithMany(p => p.InverseManager)
                    .HasForeignKey(d => d.ManagerId)
                    .HasConstraintName("usermaster_ibfk_2");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKRoleId");

                entity.HasOne(d => d.UserType)
                    .WithMany(p => p.Usermaster)
                    .HasForeignKey(d => d.UserTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKUserTypeID");
            });

            modelBuilder.Entity<Userskills>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.SkillId });

                entity.ToTable("userskills");

                entity.HasIndex(e => e.SkillId)
                    .HasName("ESSkillId_idx");

                entity.HasIndex(e => e.UserId)
                    .HasName("ESEmpId_idx");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.Property(e => e.SkillId).HasColumnType("tinyint(1)");

                entity.HasOne(d => d.Skill)
                    .WithMany(p => p.Userskills)
                    .HasForeignKey(d => d.SkillId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("userskills_ibfk_1");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Userskills)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKEUserId");
            });

            modelBuilder.Entity<Usertypemaster>(entity =>
            {
                entity.HasKey(e => e.UserTypeId);

                entity.ToTable("usertypemaster");

                entity.Property(e => e.UserTypeId).HasColumnType("tinyint(4)");

                entity.Property(e => e.UserType)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<Visadetails>(entity =>
            {
                entity.ToTable("visadetails");

                entity.HasIndex(e => e.CountryId)
                    .HasName("CountryId");

                entity.HasIndex(e => e.UserId)
                    .HasName("FKVSEmpId_idx");

                entity.Property(e => e.VisaDetailsId).HasColumnType("tinyint(1)");

                entity.Property(e => e.CountryId).HasColumnType("int(11)");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.UserId).HasColumnType("int(11)");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.Visadetails)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fkCountryVisaId");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Visadetails)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKVDUserId");
            });
        }
    }
}
