﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Bandmaster
    {
        public Bandmaster()
        {
            Usermaster = new HashSet<Usermaster>();
        }

        public sbyte BandId { get; set; }
        public string Band { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
