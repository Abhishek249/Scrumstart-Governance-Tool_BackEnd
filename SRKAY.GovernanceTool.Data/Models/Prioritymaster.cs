﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Prioritymaster
    {
        public Prioritymaster()
        {
            Projectdetails = new HashSet<Projectdetails>();
        }

        public sbyte PriorityId { get; set; }
        public string PriorityLevel { get; set; }

        public ICollection<Projectdetails> Projectdetails { get; set; }
    }
}
