﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Skillevel
    {
        public sbyte LevelId { get; set; }
        public string SkillLevel { get; set; }
    }
}
