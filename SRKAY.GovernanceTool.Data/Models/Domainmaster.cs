﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Domainmaster
    {
        public Domainmaster()
        {
            Userdomains = new HashSet<Userdomains>();
        }

        public sbyte DomainId { get; set; }
        public string DomainName { get; set; }

        public ICollection<Userdomains> Userdomains { get; set; }
    }
}
