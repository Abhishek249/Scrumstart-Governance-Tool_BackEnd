﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Departmentmaster
    {
        public Departmentmaster()
        {
            Usermaster = new HashSet<Usermaster>();
        }

        public sbyte DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
