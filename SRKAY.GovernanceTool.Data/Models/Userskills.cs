﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Userskills
    {
        public int UserId { get; set; }
        public sbyte SkillId { get; set; }

        public Skillsmaster Skill { get; set; }
        public Usermaster User { get; set; }
    }
}
