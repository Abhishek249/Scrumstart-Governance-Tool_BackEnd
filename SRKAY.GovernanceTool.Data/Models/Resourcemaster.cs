﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Resourcemaster
    {
        public Resourcemaster()
        {
            Resourcecontrol = new HashSet<Resourcecontrol>();
            Resourcepersmission = new HashSet<Resourcepersmission>();
        }

        public sbyte ResourceId { get; set; }
        public string ResourceName { get; set; }
        public string ResourceUrl { get; set; }
        public string ResourceDesc { get; set; }
        public bool IsActive { get; set; }

        public ICollection<Resourcecontrol> Resourcecontrol { get; set; }
        public ICollection<Resourcepersmission> Resourcepersmission { get; set; }
    }
}
