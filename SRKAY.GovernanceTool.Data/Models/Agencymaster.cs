﻿using System;
using System.Collections.Generic;

namespace SRKAY.GovernanceTool.Data.Models
{
    public partial class Agencymaster
    {
        public Agencymaster()
        {
            Agencycontactdetails = new HashSet<Agencycontactdetails>();
            Usermaster = new HashSet<Usermaster>();
        }

        public sbyte AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string ContactNo { get; set; }
        public string Address { get; set; }
        public int? CityId { get; set; }
        public bool IsActive { get; set; }

        public Citymaster City { get; set; }
        public ICollection<Agencycontactdetails> Agencycontactdetails { get; set; }
        public ICollection<Usermaster> Usermaster { get; set; }
    }
}
