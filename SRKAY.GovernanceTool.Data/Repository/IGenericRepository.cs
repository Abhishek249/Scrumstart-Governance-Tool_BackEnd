﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SRKAY.GovernanceTool.Data.Repository
{
    public interface IGenericRepository<T> where T : class
    {

        IQueryable<T> GetAll();
        List<T> GetAll(short numberOfPages, short pageIndex);
        List<T> FindBy(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        T Delete(T entity);
        T Edit(T entity);
        void Save();
        void SaveChanges(T entity);
        T FindById(int id);


    }
}
