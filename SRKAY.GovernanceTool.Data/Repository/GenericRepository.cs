﻿using System;
using System.Collections.Generic;

using System.Linq;
using Microsoft.EntityFrameworkCore;


namespace SRKAY.GovernanceTool.Data.Repository
{
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        private readonly DbContext _entities;
        public GenericRepository(DbContext context)
        {
            _entities = context;
        }

        public IQueryable<T> GetAll()
        {
            return _entities.Set<T>();

        }

        public List<T> GetAll(short numberOfPages, short pageIndex)
        {
            return _entities.Set<T>().Take(pageIndex * numberOfPages).Skip((pageIndex - 1) * numberOfPages).ToList();
        }

        public List<T> FindBy(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            IQueryable<T> query = _entities.Set<T>().Where(predicate);
            return query.ToList();
        }
        public virtual void Attach(T entity)
        {
            _entities.Set<T>().Attach(entity);
        }

        public virtual T Add(T entity)
        {
          return  _entities.Set<T>().Add(entity).Entity;
        }

        public virtual T Delete(T entity)
        {
           return _entities.Set<T>().Remove(entity).CurrentValues.ToObject() as T;
        }

        public virtual T Edit(T entity)
        {
            _entities.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public virtual void Save()
        {
            _entities.SaveChanges();
        }

        public virtual void SaveChanges(T entity)
        {
            if (_entities.Entry(entity).State == EntityState.Detached)
            {
                _entities.Set<T>().Attach(entity);
            }
            _entities.Entry(entity).State = EntityState.Modified;
            _entities.SaveChanges();
        }

        public virtual T FindById(int id)
        {
            return _entities.Set<T>().Find(id);
        }

    }
}
