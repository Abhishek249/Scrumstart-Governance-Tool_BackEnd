﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class UserTypeMaster
    {
        public sbyte UserTypeId { get; set; }
        public string UserType { get; set; }
    }
}
