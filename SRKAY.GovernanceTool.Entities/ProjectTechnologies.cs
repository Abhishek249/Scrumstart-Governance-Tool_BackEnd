﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class ProjectTechnologies
    {
        public int ProjectId { get; set; }
        public sbyte SkillId { get; set; }
    }
}
