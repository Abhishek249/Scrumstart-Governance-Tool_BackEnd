﻿
using System;

namespace SRKAY.GovernanceTool.Entities
{
    public class  UserMaster
    {
        public int UserId { get; set; }
        public string PayRollId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ContactNo { get; set; }
        public sbyte RoleId { get; set; }
        public float? Experience { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? UpdatedDateTime { get; set; }
        public DateTime? BirthDate { get; set; }
        public sbyte UserTypeId { get; set; }
        public sbyte? AgencyId { get; set; }
        public int LocationId { get; set; }
        public sbyte BandId { get; set; }
        public int? ManagerId { get; set; }
        public bool? IsBillable { get; set; }
        public DateTime? DateOfJoining { get; set; }
        public DateTime? DateOfExit { get; set; }
        public string Post { get; set; }
        public sbyte? DepartmentId { get; set; }
        public bool IsActive { get; set; }
    }
}
