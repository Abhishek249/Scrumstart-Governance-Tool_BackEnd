﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class ProjectDetails
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public int ClientId { get; set; }
        public bool IsActive { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public sbyte? ProjectPhaseId { get; set; }
        public DateTime AddedDateTime { get; set; }
        public int AddedBy { get; set; }
        public DateTime UpdatedDateTime { get; set; }
        public int? UpdatedBy { get; set; }
        public DateTime? PlannedDeliveryDate { get; set; }
        public sbyte PriorityId { get; set; }
    }
}
