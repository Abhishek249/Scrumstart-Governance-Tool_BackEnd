﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class CityMaster
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int StateId { get; set; }

    }
}
