﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class SkillsMaster
    {
        public sbyte SkillId { get; set; }
        public string Skill { get; set; }
    }
}
