﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class DomainMaster
    {
        public sbyte DomainId { get; set; }
        public string DomainName { get; set; }
    }
}
