﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class ProjectsRoleMaster
    {
        public sbyte ProjectRoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
