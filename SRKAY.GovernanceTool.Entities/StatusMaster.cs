﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class StatusMaster
    {
        public sbyte StatusId { get; set; }
        public string Status { get; set; }
    }
}
