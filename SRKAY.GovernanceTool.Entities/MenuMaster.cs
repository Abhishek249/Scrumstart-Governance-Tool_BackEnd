﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class MenuMaster
    {
        public sbyte MenuId { get; set; }
        public string MenuName { get; set; }
        public bool IsActive { get; set; }
        public sbyte ParentId { get; set; }
    }
}
