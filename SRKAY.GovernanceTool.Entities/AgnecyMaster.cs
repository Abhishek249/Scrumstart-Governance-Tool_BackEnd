﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class AgnecyMaster
    {
        public sbyte AgencyId { get; set; }
        public string AgencyName { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public bool IsActive { get; set; }

    }
}
