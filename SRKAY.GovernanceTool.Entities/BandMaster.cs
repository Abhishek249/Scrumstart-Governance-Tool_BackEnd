﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class BandMaster
    {
        public sbyte BandId { get; set; }
        public string Band { get; set; }
        public bool IsActive { get; set; }
    }
}
