﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class CountryMaster
    {
        public int CountryId { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public int? PhoneCode { get; set; }

    }
}
