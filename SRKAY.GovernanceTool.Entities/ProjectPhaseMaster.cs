﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class ProjectPhaseMaster
    {
        public sbyte ProjectPhaseId { get; set; }
        public string ProjectPhase { get; set; }
        public bool IsActive { get; set; }
    }
}
