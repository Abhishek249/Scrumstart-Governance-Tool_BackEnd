﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class StateMaster
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public sbyte CountryId { get; set; }
    }
}
