﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class ProjectResources
    {
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public sbyte ProjectRoleId { get; set; }
    }
}
