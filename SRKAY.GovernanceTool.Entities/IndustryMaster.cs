﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SRKAY.GovernanceTool.Entities
{
    public class IndustryMaster
    {
        public sbyte IndustryId { get; set; }
        public string IndustryType { get; set; }
    }
}
